package com.zx.video_push.configs

enum class PushBGMode {
    /**
     * 常用的模式。切换到后台后，使用采集到的最后一帧数据循环推流，保持流不断开。
     */
    BG_MODE_KEEP_LAST_FRAME,
    /**
     * 切换到后台后，使用指定的图片循环推流，保持流不断开。
     */
    BG_MODE_BITMAP_STREAMING,
    /**
     * 常用于屏幕推流，切换到后台后，会忽略 resume 和 pause 调用，观众可正常观看屏幕。
     */
    BG_MODE_NORMAL_STREAMING
}