package com.zx.video_push.configs

import android.content.Intent
import com.zx.video_push.PushClient

class PushEngineConfig private constructor(builder: Builder) {

    // 开启硬件加速，默认值为 true
    var encodeHardwareAcceleration: Boolean = true
    // 采集帧率，默认值为 30
    var captureFps: Int = 30
    // 视频采集宽度，默认值为 720
    var captureWidth: Int = 720
    // 视频采集高度，默认值为 1280
    var captureHeight: Int = 1280
    // 视频推流宽度，默认值为 720
    var videoWidth: Int = 720
    // 视频推流高度，默认值为 1280
    var videoHeight: Int = 1280
    // 推流帧率，默认值为 25。
    var videoFps: Int = 25
    // 码率，默认值为 800*1000
    var bitrate: Int = 1000*1000
    // 最大码率
    var maxBitrate: Int = bitrate*5/3
    // 最小码率
    var minBitrate: Int = bitrate*2/5
    // 横竖屏 为 true 表示竖屏，为 false 表示横屏
    var isPortrait = true
    // 采集设备,默认录屏采集
    var captureDevice = CaptureMode.DEVICE_SCREEN
    // 后台模式
    var pushBGMode = PushBGMode.BG_MODE_NORMAL_STREAMING
    // 屏幕推流使用
    var mScreenIntent: Intent? = null

    init {
        this.encodeHardwareAcceleration = builder.getEncodeHardwareAcceleration()
        this.captureFps = builder.getCaptureFps()
        this.captureWidth = builder.getCaptureWidth()
        this.captureHeight = builder.getCaptureHeight()
        this.videoFps = builder.getVideoFps()
        this.videoWidth = builder.getVideoWidth()
        this.videoHeight = builder.getVideoHeight()
        this.bitrate = builder.getBitrate()
        this.maxBitrate = builder.getMaxBitrate()
        this.minBitrate = builder.getMinBitrate()
        this.isPortrait = builder.isPortrait()
        this.captureDevice = builder.getCaptureDevice()
        this.pushBGMode = builder.getPushBGMode()
        this.mScreenIntent = builder.getScreenIntent()
    }

    object Builder {

        private var encodeHardwareAcceleration: Boolean = true
        private var captureFps: Int = 30
        private var captureWidth: Int = 720
        private var captureHeight: Int = 1280
        private var videoFps: Int = 25
        private var videoWidth: Int = 720
        private  var videoHeight: Int = 1280
        private var bitrate: Int = 1000*1000
        private  var maxBitrate: Int = bitrate*5/3
        private var minBitrate: Int = bitrate*2/5
        private var isPortrait = true
        private var captureDevice = CaptureMode.DEVICE_SCREEN
        // 后台模式
        private var pushBGMode = PushBGMode.BG_MODE_NORMAL_STREAMING
        // 屏幕推流使用
        private var mScreenIntent: Intent? = null

        fun getEncodeHardwareAcceleration(): Boolean {
            return this.encodeHardwareAcceleration
        }

        /**
         * 设置硬件加速
         */
        fun setEncodeHardwareAcceleration(encodeHardwareAcceleration: Boolean): Builder {
            this.encodeHardwareAcceleration = encodeHardwareAcceleration
            return this
        }

        fun getCaptureFps(): Int {
            return this.captureFps
        }

        /**
         * 设置采集fps
         */
        fun setCaptureFps(captureFps: Int): Builder {
            this.captureFps = captureFps
            return this
        }

        fun getCaptureWidth(): Int {
            return this.captureWidth
        }

        /**
         * 设置采集宽度
         */
        fun setCaptureWidth(captureWidth: Int): Builder {
            this.captureWidth = captureWidth
            return this
        }

        fun getCaptureHeight(): Int {
            return this.captureHeight
        }

        /**
         * 设置采集高度
         */
        fun setCaptureHeight(captureHeight: Int): Builder {
            this.captureHeight = captureHeight
            return this
        }

        fun getVideoFps(): Int {
            return this.videoFps
        }

        /**
         * 设置推流fps
         */
        fun setVideoFps(videoFps: Int): Builder {
            this.videoFps = videoFps
            return this
        }

        fun getVideoWidth(): Int {
            return this.videoWidth
        }

        /**
         * 设置推流宽度
         */
        fun setVideoWidth(videoWidth: Int): Builder {
            this.videoWidth = videoWidth
            return this
        }

        fun getVideoHeight(): Int {
            return this.videoHeight
        }

        /**
         * 设置推流高度
         */
        fun setVideoHeight(videoHeight: Int): Builder {
            this.videoHeight = videoHeight
            return this
        }

        fun getBitrate(): Int {
            return this.bitrate
        }

        /**
         * 设置码率
         */
        fun setBitrate(bitrate: Int): Builder {
            this.bitrate = bitrate
            return this
        }

        fun getMaxBitrate(): Int {
            return this.maxBitrate
        }

        /**
         * 设置最大码率
         */
        fun setMaxBitrate(maxBitrate: Int): Builder {
            this.maxBitrate = maxBitrate
            return this
        }

        fun getMinBitrate(): Int {
            return this.minBitrate
        }

        /**
         * 设置最小码率
         */
        fun setMinBitrate(minBitrate: Int): Builder {
            this.minBitrate = minBitrate
            return this
        }

        fun isPortrait(): Boolean {
            return this.isPortrait
        }

        /**
         * 设置横竖屏
         */
        fun setPortrait(isPortrait: Boolean): Builder {
            this.isPortrait = isPortrait
            return this
        }

        fun getCaptureDevice(): CaptureMode {
            return this.captureDevice
        }

        /**
         * 设置推流方式
         */
        fun setCaptureDevice(captureDevice: CaptureMode): Builder {
            this.captureDevice = captureDevice
            return this
        }

        fun getPushBGMode(): PushBGMode {
            return this.pushBGMode
        }

        /**
         * 设置后台模式
         */
        fun setPushBGMode(pushBGMode: PushBGMode): Builder {
            this.pushBGMode = pushBGMode
            return this
        }

        fun getScreenIntent(): Intent? {
            return this.mScreenIntent
        }

        fun setScreenIntent(mScreenIntent: Intent?): Builder {
            this.mScreenIntent = mScreenIntent
            return this
        }

        fun build(): PushEngineConfig {
            return PushEngineConfig(this)
        }
    }

}