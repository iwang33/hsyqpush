package com.zx.video_push.configs

enum class CaptureMode {
    /**
     * 后置摄像头
     */
    DEVICE_CAMERA_BACK,
    /**
     * 前置摄像头
     */
    DEVICE_CAMERA_FRONT,
    /**
     * 录屏
     */
    DEVICE_SCREEN,
    /**
     * 外设
     */
    DEVICE_EXTERN
}