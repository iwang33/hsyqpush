package com.zx.video_push.impl

import android.content.Context
import android.util.Log
import android.view.SurfaceView
import com.pandora.common.env.Env
import com.pandora.common.env.config.Config
import com.pandora.ttlicense2.LicenseManager
import com.pandora.ttsdk.IPublisher.*
import com.pandora.ttsdk.newapi.LiveCoreBuilder
import com.ss.avframework.engine.MediaEngineFactory
import com.ss.avframework.livestreamv2.Constants
import com.ss.avframework.livestreamv2.ILiveStream
import com.ss.avframework.livestreamv2.LiveStreamBuilder.*
import com.ss.avframework.livestreamv2.core.LiveCore
import com.ss.avframework.utils.AVLog
import com.zx.video_push.configs.CaptureMode
import com.zx.video_push.configs.PushBGMode
import com.zx.video_push.configs.PushEngineConfig
import com.zx.video_push.iInterface.IPushEngine
import com.zx.video_push.iInterface.PushCallback

class HSYQImpl: IPushEngine {
    private val TAG = "IPushEngine"

    private val liveStreamInfoListener = ILiveStream.ILiveStreamInfoListener { code1, code2, code3 ->
        when (code1) {
            Constants.MSG_INFO_STARTING_PUBLISH -> {
                Log.d(TAG, "开始推流")
            }
            Constants.MSG_INFO_RTMP_CONNECTING -> {
                Log.d(TAG, "推流连接中")
            }
            Constants.MSG_INFO_RTMP_CONNECTED -> {
                Log.d(TAG, "推流已连接")
            }
            Constants.MSG_INFO_RTMP_CONNECT_FAIL -> {
                Log.d(TAG, "推流失败")
            }
            Constants.MSG_INFO_RTMP_DISCONNECTED -> {
                Log.d(TAG, "推流断开")
            }
            Constants.MSG_INFO_STARTED_PUBLISH -> {
                Log.d(TAG, "开始推流")
            }
            Constants.MSG_INFO_STOPED_PUBLISH -> {
                Log.d(TAG, "结束推流")
            }
            Constants.MSG_INFO_RTMP_SEND_SLOW -> {
                Log.d(TAG, "网络差")
            }
            Constants.MSG_INFO_RTMP_RECONNECTING -> {
                Log.d(TAG, "重连中...")
            }
            Constants.MSG_INFO_VIDEO_ENCODER_FORMAT_CHANGED -> {
                when (code2) {
                    PUBLISHER_INFO_VIDEO_FORMAT_CHANGE_BITRATE_DOWN -> {
                        Log.d(TAG, "码率下调中...")
                    }
                    PUBLISHER_INFO_VIDEO_FORMAT_CHANGE_BITRATE_UP -> {
                        Log.d(TAG, "码率上调中...")
                    }
                    PUBLISHER_INFO_VIDEO_FORMAT_CHANGE_ENCODER_SIZE_CHANGE -> {
                        Log.d(TAG, "编码分辨率重新调整")
                    }
                    PUBLISHER_INFO_VIDEO_FORMAT_CHANGE_ENCODER_TYPE_CHANGE -> {
                        Log.d(TAG, "编码器类型发生变化")
                    }
                }
            }
            Constants.MSG_INFO_VIDEO_STARTING_CAPTURE -> {}
            Constants.MSG_INFO_VIDEO_STARTED_CAPTURE -> {
                Log.d(TAG, "视频采集已经开始")
            }
            Constants.MSG_INFO_VIDEO_STOPED_CAPTURE -> {
                Log.d(TAG, "视频采集已经停止")
            }
            Constants.MSG_INFO_AUDIO_STARTINT_CAPTURE -> {}
            Constants.MSG_INFO_AUDIO_STARTED_CAPTURE -> {
                Log.d(TAG, "音频采集开始")
            }
            Constants.MSG_INFO_AUDIO_STOPED_CAPTURE -> {
                Log.d(TAG, "音频采集停止")
            }
            Constants.MSG_INFO_AUDIO_CHANNEL_CHANGE -> {
                Log.d(TAG, "AudioCapture info: $code1,$code2,$code3")
            }
            Constants.MSG_INFO_ADM_PLAYER_STARTED -> {
                Log.d(TAG, "音频播放器开始")
            }
            Constants.MSG_INFO_ADM_PLAYER_STOPED -> {
                Log.d(TAG, "音频播放器停止")
            }
            Constants.MSG_INFO_ADM_RECORDING_START -> {
                Log.d(TAG, "开始录制")
            }
            Constants.MSG_INFO_ADM_RECORDING_STOP -> {
                Log.d(TAG, "停止录制")
            }
        }
    }

    private val liveStreamErrorListener = ILiveStream.ILiveStreamErrorListener { code1, code2, e ->
        when (code1) {
            Constants.MSG_ERROR_VIDEO_CAPTURE -> {
                Log.d(TAG, "视频采错误")
            }
            Constants.MSG_ERROR_AUDIO_CAPTURE -> {
                Log.d(TAG, "音频采集错误(" + e.cause + ")")
            }
            Constants.MSG_ERROR_RTMP -> {}
            Constants.MSG_ERROR_VIDEO_ENCODER,
            Constants.MSG_ERROR_AUDIO_ENCODER -> {
                Log.d(TAG, "推流错误，$code2 $e")
            }
            Constants.MSG_ERROR_ADM_RECORDING,
            Constants.MSG_ERROR_ADM_PLAYER,
            Constants.MSG_STATUS_EXCEPTION -> {

            }
        }

    }

    private val mLicenseCallback: LicenseManager.Callback = object : LicenseManager.Callback {
        override fun onLicenseLoadSuccess(licenseUri: String, licenseId: String) {
            Log.d(TAG, "onLicenseLoadSuccess ::: licenseId: $licenseId")
        }

        override fun onLicenseLoadError(licenseUri: String, e: Exception, retryAble: Boolean) {}
        override fun onLicenseLoadRetry(licenseUri: String) {}
        override fun onLicenseUpdateSuccess(licenseUri: String, licenseId: String) {
            Log.d(TAG, "onLicenseUpdateSuccess ::: licenseId: $licenseId")
        }

        override fun onLicenseUpdateError(licenseUri: String, e: Exception, retryAble: Boolean) {}
        override fun onLicenseUpdateRetry(licenseUri: String) {}
    }

    private var pushEngine: LiveCore? = null
    private var callback: PushCallback? = null
    private var builder: LiveCoreBuilder? = null
    private var config: PushEngineConfig? = null

    override fun init(context: Context) {
        Env.init(
            Config.Builder()
                .setApplicationContext(context)
                .setAppID("507117")
                .setAppName("云手机分享")
                .setAppVersion("1.4.11.100") // 合法版本号应大于、等于 2 个分隔符，如："1.3.2"
                .setAppChannel("Box")
                .setLicenseUri("assets:///license/live.lic")
                .setLicenseCallback(mLicenseCallback)//License 加载的状态回调
                .build())
        LicenseManager.turnOnLogcat(true)
    }

    override fun registerCallback(callback: PushCallback) {
        this.callback = callback
    }

    override fun createLiveCoreEngine(config: PushEngineConfig) {
        this.config = config

        builder = LiveCoreBuilder().apply {

            videoCaptureDevice = when(config.captureDevice) {
                CaptureMode.DEVICE_CAMERA_BACK -> {
                    VIDEO_CAPTURE_DEVICE_CAM_BACK
                }
                CaptureMode.DEVICE_CAMERA_FRONT -> {
                    VIDEO_CAPTURE_DEVICE_CAM_FRONT
                }
                CaptureMode.DEVICE_SCREEN -> {
                    VIDEO_CAPTURE_DEVICE_SCREEN
                }
                CaptureMode.DEVICE_EXTERN -> {
                    VIDEO_CAPTURE_DEVICE_EXTERN
                }
            }

            if (config.captureDevice == CaptureMode.DEVICE_SCREEN) {
                screenCaptureIntent = config.mScreenIntent
            }

            bgMode = when(config.pushBGMode) {
                PushBGMode.BG_MODE_KEEP_LAST_FRAME -> {
                    BG_MODE_KEEP_LAST_FRAME
                }
                PushBGMode.BG_MODE_BITMAP_STREAMING -> {
                    BG_MODE_BITMAP_STREAMING
                }
                PushBGMode.BG_MODE_NORMAL_STREAMING -> {
                    BG_MODE_NORMAL_STREAMING
                }
            }

            isEnableVideoEncodeAccelera = config.encodeHardwareAcceleration
            videoCaptureFps = config.captureFps
            videoFps = config.videoFps
            if (config.isPortrait) {
                videoCaptureWidth = config.captureWidth
                videoCaptureHeight = config.captureHeight
                videoWidth = config.videoWidth
                videoHeight = config.videoHeight
            } else {
                videoCaptureWidth = config.captureHeight
                videoCaptureHeight = config.captureWidth
                videoWidth = config.videoHeight
                videoHeight = config.videoWidth
            }
            videoBitrate = config.bitrate
            videoMaxBitrate = config.maxBitrate
            videoMinBitrate = config.minBitrate
            //H265
//            videoEncoder = VIDEO_ENCODER_BYTEVC1
        }

        pushEngine = builder?.liveCoreEngine?.liveCore
        pushEngine?.setInfoListener(liveStreamInfoListener)
        pushEngine?.setErrorListener(liveStreamErrorListener)

        //other
        MediaEngineFactory.setLogLevel(AVLog.VERBOSE)
    }

    override fun setDisplay(surfaceView: SurfaceView) {
        pushEngine?.setDisplay(surfaceView)
    }

    override fun startVideoCapture() {
        pushEngine?.startVideoCapture()
    }

    override fun stopVideoCapture() {
        pushEngine?.stopVideoCapture()
    }

    override fun startAudioCapture() {
        pushEngine?.startAudioCapture()
    }

    override fun stopAudioCapture() {
        pushEngine?.stopAudioCapture()
    }

    override fun start(pushUrl: String) {
        pushEngine?.start(pushUrl)
    }

    override fun resume() {
        pushEngine?.resume()
    }

    override fun pause() {
        pushEngine?.pause()
    }

    override fun stop() {
        pushEngine?.stop()
    }

    override fun release() {
        pushEngine?.release()
    }

    override fun switchVideoCapture(mode: CaptureMode) {
        when(mode) {
            CaptureMode.DEVICE_CAMERA_BACK -> {
                pushEngine?.switchVideoCapture(VIDEO_CAPTURE_DEVICE_CAM_BACK)
            }
            CaptureMode.DEVICE_CAMERA_FRONT -> {
                pushEngine?.switchVideoCapture(VIDEO_CAPTURE_DEVICE_CAM_FRONT)
            }
            CaptureMode.DEVICE_SCREEN -> {
                pushEngine?.switchVideoCapture(VIDEO_CAPTURE_DEVICE_SCREEN)
            }
            CaptureMode.DEVICE_EXTERN -> {
                pushEngine?.switchVideoCapture(VIDEO_CAPTURE_DEVICE_EXTERN)
            }
        }
    }

    override fun enableMirror(mirror: Boolean, isHorizontal: Boolean) {
        pushEngine?.enableMirror(mirror, isHorizontal)
    }

    override fun switchOrientation(isPortrait: Boolean) {
        config?.let {
            if (isPortrait) {
                builder?.videoCaptureWidth = it.captureWidth
                builder?.videoCaptureHeight = it.captureHeight
                builder?.videoWidth = it.videoWidth
                builder?.videoHeight = it.videoHeight
            } else {
                builder?.videoCaptureWidth = it.captureHeight
                builder?.videoCaptureHeight = it.captureWidth
                builder?.videoWidth = it.videoHeight
                builder?.videoHeight = it.videoWidth
            }
        }
    }

    override fun setAudioMute(mute: Boolean) {
        pushEngine?.setAudioMute(mute)
    }

    override fun setBgMode(mode: PushBGMode) {
        when(mode) {
            PushBGMode.BG_MODE_KEEP_LAST_FRAME -> {
                builder?.bgMode = BG_MODE_KEEP_LAST_FRAME
            }
            PushBGMode.BG_MODE_BITMAP_STREAMING -> {
                builder?.bgMode = BG_MODE_BITMAP_STREAMING
            }
            PushBGMode.BG_MODE_NORMAL_STREAMING -> {
                builder?.bgMode = BG_MODE_NORMAL_STREAMING
            }
        }
    }


}