package com.zx.video_push.iInterface

import android.content.Context
import android.view.SurfaceView
import com.zx.video_push.configs.CaptureMode
import com.zx.video_push.configs.PushBGMode
import com.zx.video_push.configs.PushEngineConfig

interface IPushEngine {
    /**
     * 初始化
     */
    fun init(context: Context)

    /**
     * 注册回调
     */
    fun registerCallback(callback: PushCallback)

    /**
     * 创建推流引擎
     */
    fun createLiveCoreEngine(config: PushEngineConfig)

    /**
     * 设置预览窗口
     */
    fun setDisplay(surfaceView: SurfaceView)

    /**
     * 开始视频采集
     */
    fun startVideoCapture()

    /**
     * 停止视频采集
     */
    fun stopVideoCapture()

    /**
     * 开始音频采集
     */
    fun startAudioCapture()

    /**
     * 停止音频采集
     */
    fun stopAudioCapture()

    /**
     * 开始推流
     */
    fun start(pushUrl: String)

    /**
     * 恢复推流
     */
    fun resume()

    /**
     * 暂停推流
     */
    fun pause()

    /**
     * 停止推流
     */
    fun stop()

    /**
     * 释放
     */
    fun release()

    /**
     * 切换前后摄像头
     */
    fun switchVideoCapture(mode: CaptureMode)

    /**
     * 镜像设置
     * @param mirror 为 true 表示开启镜像，为 false 表示关闭镜像
     * @param isHorizontal 为 true 表示水平翻转，为 false 表示垂直翻转
     */
    fun enableMirror(mirror: Boolean, isHorizontal: Boolean)

    /**
     * 横竖屏切换
     * @param isPortrait 为 true 表示竖屏，为 false 表示横屏
     */
    fun switchOrientation(isPortrait: Boolean)

    /**
     * 麦克风静音
     * @param mute 参数为 true 表示开启静音，为 false 表示关闭静音
     */
    fun setAudioMute(mute: Boolean)

    /**
     * 后台模式
     */
    fun setBgMode(mode: PushBGMode)


}