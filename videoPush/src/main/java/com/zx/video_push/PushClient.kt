package com.zx.video_push

import android.content.Context
import android.view.SurfaceView
import com.zx.video_push.configs.CaptureMode
import com.zx.video_push.configs.PushBGMode
import com.zx.video_push.configs.PushChannel
import com.zx.video_push.configs.PushEngineConfig
import com.zx.video_push.iInterface.IPushEngine
import com.zx.video_push.iInterface.PushCallback
import com.zx.video_push.impl.HSYQImpl

object PushClient {

    private var mPushEngine: IPushEngine? = null

    private var channel: PushChannel = PushChannel.HSYQ

    /**
     * 初始化
     */
    fun init(context: Context, channel: PushChannel) {
        this.channel = channel
        getPushDriver()?.init(context)
    }

    /**
     * 注册回调
     */
    fun registerCallback(callback: PushCallback) {
        getPushDriver()?.registerCallback(callback)
    }

    /**
     * 创建推流引擎
     */
    fun createLiveCoreEngine(config: PushEngineConfig) {
        getPushDriver()?.createLiveCoreEngine(config)
    }

    /**
     * 设置预览窗口
     */
    fun setDisplay(surfaceView: SurfaceView) {
        getPushDriver()?.setDisplay(surfaceView)
    }

    /**
     * 开始视频采集
     */
    fun startVideoCapture() {
        getPushDriver()?.startVideoCapture()
    }

    /**
     * 开始音频采集
     */
    fun startAudioCapture() {
        getPushDriver()?.startAudioCapture()
    }

    /**
     * 开始推流
     */
    fun start(pushUrl: String) {
        getPushDriver()?.start(pushUrl)
    }

    /**
     * 恢复推流
     */
    fun resume() {
        getPushDriver()?.resume()
    }

    /**
     * 暂停推流
     */
    fun pause() {
        getPushDriver()?.pause()
    }

    /**
     * 释放
     */
    fun release() {
        //停止音视频采集。
        getPushDriver()?.stopVideoCapture();
        getPushDriver()?.stopAudioCapture();
        //停止推流。
        getPushDriver()?.stop()
        //释放引擎。
        getPushDriver()?.release()
    }

    /**
     * 切换前后摄像头
     */
    fun switchVideoCapture(mode: CaptureMode) {
        getPushDriver()?.switchVideoCapture(mode)
    }

    /**
     * 镜像设置
     * @param mirror 为 true 表示开启镜像，为 false 表示关闭镜像
     * @param isHorizontal 为 true 表示水平翻转，为 false 表示垂直翻转
     */
    fun enableMirror(mirror: Boolean, isHorizontal: Boolean) {
        getPushDriver()?.enableMirror(mirror, isHorizontal)
    }

    /**
     * 横竖屏切换
     * @param isPortrait 为 true 表示竖屏，为 false 表示横屏
     */
    fun switchOrientation(isPortrait: Boolean) {
        getPushDriver()?.switchOrientation(isPortrait)
    }

    /**
     * 麦克风静音
     * @param mute 参数为 true 表示开启静音，为 false 表示关闭静音
     */
    fun setAudioMute(mute: Boolean) {
        getPushDriver()?.setAudioMute(mute)
    }

    /**
     * 后台模式
     */
    fun setBgMode(mode: PushBGMode) {
        getPushDriver()?.setBgMode(mode)
    }

    @Synchronized
    private fun <T : IPushEngine> create(cls: Class<T>): IPushEngine? {
        if (mPushEngine == null) {
            mPushEngine = cls.newInstance()
        }
        return mPushEngine
    }

    private fun getPushDriver(): IPushEngine? {
        return when (channel) {
            PushChannel.HSYQ -> {
                create(HSYQImpl::class.java)
            }
            else -> {
                null
            }
        }
    }
}