package com.zx.push.utils

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

object ScopeUtil {
    fun <T> launchIO(
        io: () -> T,
        ok: (T) -> Unit = {}
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val t = io.invoke()
            MainScope().launch {
                ok(t)
            }
        }
    }

    fun launch(
        context: CoroutineContext = Dispatchers.Main,
        block: suspend CoroutineScope.(j: Job) -> Unit
    ) {
        val job = Job()
        val scope = CoroutineScope(job)

        scope.launch(context) {
            block.invoke(this, job)
        }
    }
}