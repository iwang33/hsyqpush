package com.zx.push.utils

import android.app.Application
import android.content.Context


object AppCore {
    private lateinit var mApp: Application

    fun init(app : Application){
        mApp = app
    }

    fun context() : Context{
        return mApp
    }
}