package com.zx.push.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

fun Any.scope(
    context: CoroutineContext = Dispatchers.Main,
    job: suspend CoroutineScope.() -> Unit
) {
    ScopeUtil.launch(context) {
        job.invoke(this)
    }
}

fun Any.scopeIo(
    context: CoroutineContext = Dispatchers.IO,
    job: suspend CoroutineScope.(j: Job) -> Unit
) {
    ScopeUtil.launch(context) {
        job.invoke(this, it)
    }
}