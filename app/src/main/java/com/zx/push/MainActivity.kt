package com.zx.push

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.projection.MediaProjectionManager
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.zx.cp_share.R
import com.zx.video_push.PushClient
import com.zx.video_push.configs.CaptureMode
import com.zx.video_push.configs.PushEngineConfig


class MainActivity : AppCompatActivity() {
    val REQUESTCODE_FROM_PROJECTION_SERVICE = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.btn_send).setOnClickListener {
            toGetScreenRecordPermission()
        }
        checkPermission(1001)
    }

    private fun checkPermission(request: Int): Boolean {
        val permissions = arrayOf<String>(
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.MODIFY_AUDIO_SETTINGS,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val permissionList: MutableList<String> = ArrayList()
        for (permission in permissions) {
            val granted = ContextCompat.checkSelfPermission(
                this,
                permission
            ) == PackageManager.PERMISSION_GRANTED
            if (granted) continue
            permissionList.add(permission)
        }
        if (permissionList.isEmpty()) return true
        val permissionsToGrant = permissionList.toTypedArray()
        ActivityCompat.requestPermissions(this, permissionsToGrant, request)
        return false
    }

    private fun toGetScreenRecordPermission() {
        val mgr = getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager
        val intent = mgr.createScreenCaptureIntent()
        startActivityForResult(intent, REQUESTCODE_FROM_PROJECTION_SERVICE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUESTCODE_FROM_PROJECTION_SERVICE && resultCode == RESULT_OK) {
            PushClient.createLiveCoreEngine(
                PushEngineConfig.Builder
                    .setScreenIntent(data)
//                    .setCaptureDevice(CaptureMode.DEVICE_CAMERA_BACK)
                    .build())
            PushClient.setDisplay(findViewById(R.id.surfaceView))
            PushClient.startVideoCapture()
            PushClient.startAudioCapture()
            PushClient.start("http://pushstream.zxzt168.com/cpshare/bdlive.sdp?volcTime=1687964450&volcSecret=38ec8f2b57a4ab572c83973b335f4122")
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}