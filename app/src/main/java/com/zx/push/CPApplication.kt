package com.zx.push

import android.app.Application
import com.zx.push.utils.AppCore
import com.zx.video_push.PushClient
import com.zx.video_push.configs.PushChannel

class CPApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        AppCore.init(this)

        PushClient.init(this, PushChannel.HSYQ)
    }
}